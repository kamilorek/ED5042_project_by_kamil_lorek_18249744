package com.example.meetingalarm;

import android.os.Bundle;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

//Notification.java made by Kamil Lorek, ID 18249744

public class Notification extends AppCompatActivity {
    TextView textView;

    //Displaying activity_notification.xml on screen

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        textView = findViewById(R.id.the_message);
        Bundle bundle = getIntent().getExtras();
        textView.setText(bundle.getString("message"));
    }
}
