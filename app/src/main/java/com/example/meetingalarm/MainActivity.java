package com.example.meetingalarm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.example.meetingalarm.Adapter.Adapter;
import com.example.meetingalarm.Database.DatabaseClass;
import com.example.meetingalarm.Database.EntityClass;
import java.util.List;

//MainActivity.java made by Kamil Lorek, ID 18249744

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //Defining
    Button createEvent;
    Adapter adapter;
    RecyclerView recyclerview;
    DatabaseClass databaseClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Getting connected with activity_main.xml

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createEvent = findViewById(R.id.btn_createEvent);
        recyclerview = findViewById(R.id.recyclerview);
        createEvent.setOnClickListener(this);
        databaseClass = DatabaseClass.getDatabase(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAdapter();
    }

    private void setAdapter() {
        List<EntityClass> classList = databaseClass.DaoEvent().getAllData();
        adapter = new Adapter(getApplicationContext(), classList);
        recyclerview.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        if (view == createEvent) {
            goToCreateEventActivity();
        }
    }

    private void goToCreateEventActivity() {
        Intent intent = new Intent(getApplicationContext(), CreateEvent.class);
        startActivity(intent);
    }
}