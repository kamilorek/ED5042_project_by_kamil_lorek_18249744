package com.example.meetingalarm;

import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;

//BootReceiver.java made by Kamil Lorek, ID 18249744

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intent1 = new Intent(context, MainActivity.class);
        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent1);
    }
}
