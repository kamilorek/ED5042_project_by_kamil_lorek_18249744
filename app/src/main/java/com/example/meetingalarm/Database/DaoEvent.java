package com.example.meetingalarm.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;

//DaoEvent.java made by Kamil Lorek, ID 18249744

@Dao
public interface DaoEvent {

    @Insert
    void insertAll(EntityClass entityClass);

    //Getting table from the database
    @Query("SELECT * FROM myTable")
    List<EntityClass> getAllData();
}

