package com.example.meetingalarm.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

//DatabaseClass.java made by Kamil Lorek, ID 18249744

@Database(entities = {EntityClass.class}, version = 1)
public abstract class DatabaseClass extends RoomDatabase {

    private static DatabaseClass INSTANCE;

    public abstract DaoEvent DaoEvent();

    public static DatabaseClass getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (DatabaseClass.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), DatabaseClass.class, "product_database").allowMainThreadQueries().build();
                }
            }
        }

        return INSTANCE;
    }
}